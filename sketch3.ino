int pinDirA = 12; //Pin number for Motor A Direction
int pinSpeedA = 3; //Pin number for Motor A Speed
int pinBreakA = 9; //Pin number for Motor A Break
int pinDirB = 13;   //Pin number for Motor B Direction
int pinSpeedB = 11; //Pin number for Motor B Speed
int pinBreakB = 8;  //Pin number for Motor B Break
int button_On_cont1 = 7;
int button_On_no1=2;
int button_Off_cont2 =5;
int button_Off_no2=6;
int off = 4;
int state1=0;
int state2=0;
int val1 =0;
int old_val1=0;
int val2 =0;
int old_val2=0;
int flag1=0;
int flag2=0;
int vval1=0;
int old_vval1=0;
int vval2=0;
int old_vval2=0;
void setup() {
  pinMode(pinDirA, OUTPUT);   // Sets the direction mode for motor A
  pinMode(pinSpeedA, OUTPUT);   //Sets the speed of Motor A
  pinMode(pinBreakA,OUTPUT);  //Enables or disables the break of motor A
  pinMode(button_On_cont1,INPUT);  //Checks the status of button that is continous till end (for closing state)
  pinMode(button_On_no1, INPUT);  //check the status of button that is "On" till such time it is pressed (for closing state)
  pinMode(button_Off_cont2,INPUT);  //Checks the status of button that is continous till end (for opening state)
  pinMode(button_Off_no2,INPUT);  //check the status of button that is "On" till such time it is pressed (for opening state)
  pinMode(pinDirB, OUTPUT);  //Sets the direction mode for Motor B
  pinMode(pinSpeedB, OUTPUT);  //Sets the speed mode for Motor B
  pinMode(pinBreakB,OUTPUT); //Enables or disables the break for Motor B
}
void loop() {
  if(digitalRead(button_On_no1)==HIGH)
  {
    digitalWrite(pinBreakA, LOW);
    digitalWrite(pinDirA, HIGH);
    analogWrite(pinSpeedA,200);
    digitalWrite(pinBreakB, LOW);
    digitalWrite(pinDirB, HIGH);
    analogWrite(pinSpeedB,200);
  }
   else
  {
    digitalWrite(pinBreakA,HIGH);
    digitalWrite(pinBreakB,HIGH);
} 
  buttonMode(button_frw_cont1, state1, val1, old_val1);
  check(button_On_no1, vval1, state1);
  check2(button_rew_cont1, vval2,state1,state2);
  check(button_Off_no2, flag1,state1); 
  check(button_frw_cont1, vval2,state2);
  check (off, flag2, state1);  
  if(state1==1)
  {
     digitalWrite(pinBreakA, LOW);
     digitalWrite(pinDirA, HIGH);
     digitalWrite(pinBreakB, LOW);
     digitalWrite(pinDirB, HIGH);     
    analogWrite(pinSpeedA, 200);
    analogWrite(pinSpeedB, 200);
       }   
  else
  {
    digitalWrite(pinBreakB,HIGH);
    digitalWrite(pinBreakA,HIGH);
 
  }
  /*buttonMode(button_On_cont1, state1, val1, old_val1);
  check(button_On_no1, vval1, state1);
  check(button_Off_cont2, vval2,state1);
  check(button_Off_no2, flag1,state1); 
  check (off, flag2, state1);  
  if(state1==1)
  {
     digitalWrite(pinBreakA, LOW);
     digitalWrite(pinDirA, HIGH);
     digitalWrite(pinBreakB, LOW);
     digitalWrite(pinDirB, HIGH);     
    analogWrite(pinSpeedA, 200);
    analogWrite(pinSpeedB, 200);
       }   
  else
  {
    digitalWrite(pinBreakB,HIGH);
    digitalWrite(pinBreakA,HIGH);
 
  }*/
  if(digitalRead(button_Off_no2)==HIGH)
  {
    digitalWrite(pinBreakB, LOW);
    digitalWrite(pinDirB, LOW);
    analogWrite(pinSpeedB,200);
    digitalWrite(pinBreakA, LOW);
    digitalWrite(pinDirA, LOW);
    analogWrite(pinSpeedA,200);
  }
  else
  {
    digitalWrite(pinBreakA,HIGH);
    digitalWrite(pinBreakB,HIGH);
} 
buttonMode(button_rew_cont1, state2, val2, old_val2);
  check(button_On_no1, vval1, state2);
  check2(button_frw_cont1, vval2,state2,state1);
  check (off, vval1, state2);  
  check(button_Off_no2, flag2,state2);
  check(button_rew_cont1, vval2,state2);
  if(state2==1)
  {
     digitalWrite(pinBreakA, LOW);
     digitalWrite(pinDirA, LOW);
     digitalWrite(pinBreakB, LOW);
     digitalWrite(pinDirB, LOW);
    analogWrite(pinSpeedA, 200);
    analogWrite(pinSpeedB, 200);
  }
  else
  {
    digitalWrite(pinBreakA,HIGH);
    digitalWrite(pinBreakB,HIGH);
 
  }
  /*buttonMode(button_Off_cont2, state2, val2, old_val2);
  check(button_On_no1, vval1, state2);
  check(button_On_cont1, vval2,state2);
  check (off, vval1, state2);  
  check(button_Off_no2, flag2,state2);
  if(state2==1)
  {
     digitalWrite(pinBreakA, LOW);
     digitalWrite(pinDirA, LOW);
     digitalWrite(pinBreakB, LOW);
     digitalWrite(pinDirB, LOW);
    analogWrite(pinSpeedA, 200);
    analogWrite(pinSpeedB, 200);
  }
  else
  {
    digitalWrite(pinBreakA,HIGH);
    digitalWrite(pinBreakB,HIGH);
 
  }*/
}
void buttonMode(int pin, int& state, int val, int& old_val)
{
  val=digitalRead(pin);
  if((val==HIGH)&&(old_val==0))
  {
    state = 1-state;
    delay(100);
  }
  old_val=val;
}
void check (int pin, int &val, int &val2)
{
  val = digitalRead(pin);
  if(val==HIGH)
  val2=0;
}
void check2 (int pin, int &val, int &val2, int&val3)
{
  val = digitalRead(pin);
  if(val==HIGH){
  val2=0;
  val3=0;}
}
